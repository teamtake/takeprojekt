package pl.kurs.competition;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Competitions {
	private List<Competition> competitions = new ArrayList<Competition>();

	public Competitions(List<Competition> competitions) {
		super();
		this.competitions = competitions;
	}

	public Competitions() {	}	
	public List<Competition> getCompetitions() { return competitions;}
	public void setCompetitions(List<Competition> competitions) { this.competitions = competitions;}
}