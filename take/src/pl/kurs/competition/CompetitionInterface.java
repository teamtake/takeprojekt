package pl.kurs.competition;

import javax.ejb.Local;

@Local
public interface CompetitionInterface {

	public abstract String create(Competition competition);

	public abstract Competition find(int idc);

	public abstract Competitions get();

	public abstract String update(Competition competition);

	public abstract void delete(int idc);

}