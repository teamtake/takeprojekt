package pl.kurs.competition;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class CompetitionEJB {
	
	@PersistenceContext(name="competition")
	EntityManager manager;

	
	public void create(Competition competition) {
		System.out.println("Creating competition!");
		manager.persist(competition);
	}

	public void delete(int idc) {
		Competition competition = manager.find(Competition.class, idc);
		System.out.println("Delete competition!");
		manager.remove(competition);
	}

	public List<Competition> findByMake(String make) {
		Query q = manager.createQuery("select c from Competition c where c.make like :make");
		q.setParameter("name", make);
		@SuppressWarnings("unchecked")
		List<Competition> lista =q.getResultList();
		return lista;
	}

	public Competition find(int idc) {
		return manager.find(Competition.class, idc);
	}

	public List<Competition> get() {
		Query q = manager.createQuery("select c from Competition c");
		@SuppressWarnings("unchecked")
		List<Competition> list = q.getResultList();
		return list;
	}

	public void update(Competition competition) {
		competition = manager.merge(competition);
		System.out.println("Updating competition!");
	}
}
