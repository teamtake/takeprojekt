package pl.kurs.competition;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import pl.kurs.race.Race;


@Entity
@XmlRootElement(name="Competition")
public class Competition implements Serializable{
	private static final long serialVersionUID = 1L;
	int id;
	String name;
	String beginDate;
	String endDate;
	Set<Race> races = new HashSet<Race>();

	@Id
	@GeneratedValue
	@XmlAttribute
	public int getId() { return id;}
	public void setId(int id) { this.id = id;}
	public String getName() { return name;}
	public void setName(String name) { this.name = name;}
	public String getBeginDate() { return beginDate;}
	public void setBeginDate(String beginDate) { this.beginDate = beginDate;}
	public String getEndDate() { return endDate;}
	public void setEndDate(String endDate) { this.endDate = endDate;}
	
	@OneToMany(fetch=FetchType.EAGER, cascade = {CascadeType.ALL})
	public Set<Race> getRaces() {
		return races;
	}
	
	public void setRaces(Set<Race> races) {
		this.races = races;
	}
	
}