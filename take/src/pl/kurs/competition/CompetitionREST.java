package pl.kurs.competition;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/competition")
//@Consumes({ "application/json" })
//@Produces({ "application/json" })

@Consumes({ "application/xml" })
@Produces({ "application/xml" })

public class CompetitionREST implements CompetitionInterface {

	@EJB
	CompetitionEJB bean;

	@Override
	@POST
	@Path("/create")
	public String create(Competition competition) {
		bean.create(competition);
		return "competition created!";
	}

	@Override
	@GET
	@Path("/find/{idc}")
	public Competition find(@PathParam("idc") int idc) {
		Competition competition = bean.find(idc);
		return competition;
	}

	@Override
	@GET
	@Path("/get")
	public Competitions get() {
		List<Competition> lcompetitions = bean.get();
		Competitions competitions = new Competitions(lcompetitions);
		return competitions;
	}

	@Override
	@POST
	@Path("/update")
	public String update(Competition competition) {
		try {
			bean.update(competition);
			return "competition updated!";
		} catch (Exception e) {
			e.printStackTrace();
			return "competition not updated :(";
		}
	}


	@Override
	@GET
	@Path("/delete/{idc}")
	public void delete(@PathParam("idc") int idc) {
		bean.delete(idc);
	}

}
