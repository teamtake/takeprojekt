package pl.kurs.result;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Result implements Serializable{
	private static final long serialVersionUID = 1L;
	int idResult;
	double result;
	int position ;
	boolean accomplished;

	@Id
	@GeneratedValue
	@XmlAttribute
	public int getId() { return idResult;}
	public void setId(int id) { this.idResult = id;}
	public Double getResult() {	return result;}
	public void setResult(Double result) {	this.result = result;}
	public int getPosition() {return position;}
	public void setPosition(int position) {	this.position = position;}
	public boolean hasAccomplished() {	return accomplished;}
	public void setAccomplished(boolean accomplished) {	this.accomplished = accomplished;}

}
