package pl.kurs.race;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Races {
	private List<Race> races = new ArrayList<Race>();

	public Races(List<Race> races) {
		super();
		this.races = races;
	}

	public Races() {	}	
	public List<Race> getRaces() { return races;}
	public void setRaces(List<Race> races) { this.races = races;}
}
