package pl.kurs.race;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import pl.kurs.player.Player;
import pl.kurs.result.Result;


@Entity
@XmlRootElement
public class Race implements Serializable{
	private static final long serialVersionUID = 1L;
	int idRace;
	String name;
	String date;
	Set<Result> results = new HashSet<Result>();
	Set<Player> players = new HashSet<Player>();

	@Id
	@GeneratedValue
	@XmlAttribute
	public int getId() { return idRace;}
	public void setId(int id) { this.idRace = id;}
	
	public String getName() { return name;}
	public void setName(String name) { this.name = name;}
	public String getDate() { return date;}
	public void setDate(String date) { this.date = date;}
	
	@OneToMany(fetch=FetchType.EAGER,cascade = {CascadeType.ALL})
	public Set<Result> getResults() {
		return results;
	}
	
	public void setResults(Set<Result> results){
		this.results = results;
	}
	
    @ManyToMany(fetch=FetchType.EAGER,cascade = {CascadeType.ALL}) 
    @JoinTable(name="race_result", joinColumns=@JoinColumn(name="id_race"), inverseJoinColumns=@JoinColumn(name="id_result"))  
    public Set<Player> getPlayers()  {  
        return players;  
    }  
    
    public void setPlayers(Set<Player> players)  {  
        this.players = players;  
    }
}
