package pl.kurs.race;

import pl.kurs.race.Race;
import pl.kurs.race.Races;

public interface RaceInterface {

	public abstract String create(Race race);

	public abstract Race find(int idc);

	public abstract Races get();

	public abstract String update(Race race);

	public abstract void delete(int idc);
}
