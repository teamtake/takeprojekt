package pl.kurs.race;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import pl.kurs.race.RaceInterface;
import pl.kurs.race.Race;
import pl.kurs.race.RaceEJB;
import pl.kurs.race.Races;

@Path("/race")
//@Consumes({ "application/json" })
//@Produces({ "application/json" })

@Consumes({ "application/xml" })
@Produces({ "application/xml" })

public class RaceREST implements RaceInterface{
	@EJB
	RaceEJB bean;

	@Override
	@POST
	@Path("/create")
	public String create(Race race) {
		bean.create(race);
		return "Race created!";
	}

	@Override
	@GET
	@Path("/find/{idc}")
	public Race find(@PathParam("idc") int idc) {
		Race race = bean.find(idc);
		return race;
	}

	@Override
	@GET
	@Path("/get")
	public Races get() {
		List<Race> lraces = bean.get();
		Races races = new Races(lraces);
		return races;
	}

	@Override
	@POST
	@Path("/update")
	public String update(Race race) {
		try {
			bean.update(race);
			return "race updated!";
		} catch (Exception e) {
			e.printStackTrace();
			return "race not updated :(";
		}
	}


	@Override
	@GET
	@Path("/delete/{idc}")
	public void delete(@PathParam("idc") int idc) {
		bean.delete(idc);
	}
}
