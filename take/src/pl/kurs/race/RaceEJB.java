package pl.kurs.race;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pl.kurs.race.Race;

@Stateless
public class RaceEJB {
	
	@PersistenceContext(name="race")
	EntityManager manager;

	
	public void create(Race race) {
		System.out.println("Creating race!");
		manager.persist(race);
	}

	public void delete(int idc) {
		Race race = manager.find(Race.class, idc);
		System.out.println("Deleting race!");
		manager.remove(race);
	}

	public List<Race> findByMake(String make) {
		Query q = manager.createQuery("select c from Race c where c.make like :make");
		q.setParameter("name", make);
		@SuppressWarnings("unchecked")
		List<Race> lista = q.getResultList();
		return lista;
	}

	public Race find(int idc) {
		return manager.find(Race.class, idc);
	}

	public List<Race> get() {
		Query q = manager.createQuery("select c from Race c");
		@SuppressWarnings("unchecked")
		List<Race> list = q.getResultList();
		return list;
	}

	public void update(Race race) {
		race = manager.merge(race);
		System.out.println("Updating race!");
	}

}
