package pl.kurs.player;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import pl.kurs.result.Result;

@Entity
@XmlRootElement
public class Player implements Serializable{
	private static final long serialVersionUID = 1L;
	int id;
	String firstname;
	String surname;
	int age;
	int sex;
	Set<Result> results = new HashSet<Result>();

	@Id
	@GeneratedValue
	@XmlAttribute
	public int getId() { return id;}
	public void setId(int id) { this.id = id;}
	public String getFirstname() { return firstname;}
	public void setFirstname(String firstname) { this.firstname = firstname;}
	public String getSurname() { return surname;}
	public void setSurname(String surname) { this.surname = surname;}
	public int getAge() { return age;}
	public void setAge(int age) { this.age = age;}
	
	@OneToMany
	public Set<Result> getResults() {
		return results;
	}
	
	public void setResults(Set<Result> results){
		this.results = results;
	}
	
}
