package pl.kurs.player;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Players  {
	private List<Player> players = new ArrayList<Player>();

	public Players(List<Player> players) {
		super();
		this.players = players;
	}

	public Players() {	}
	
	public List<Player> getPlayers() {
		return players;
	}
	
	public void setPlayers(List<Player> players) {
		this.players = players;
	}
}
