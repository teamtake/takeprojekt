Dokumentacja projektu Zawody Pływackie
[logo]: DB-scheme.png "Schemat bazy danych"

# Założenia projektu

Prosta aplikacja do zarządzania wynikami zawodów pływackich.
Funkcjonalności:
-Dodawanie zawodów
-Dodawanie wyścigów w ramach zawodów
-Dodawanie zawodników i ich wyników w ramach każdego wyścigu


# Schemat bazy danych
![alt text][logo]

# Encje

### Competition
```java
@Entity
@XmlRootElement(name="Competition")
public class Competition implements Serializable{
	private static final long serialVersionUID = 1L;
	int id;
	String name;
	String beginDate;
	String endDate;
	Set<Race> races = new HashSet<Race>();

	@Id
	@GeneratedValue
	@XmlAttribute
	public int getId() { return id;}
	public void setId(int id) { this.id = id;}
	public String getName() { return name;}
	public void setName(String name) { this.name = name;}
	public String getBeginDate() { return beginDate;}
	public void setBeginDate(String beginDate) { this.beginDate = beginDate;}
	public String getEndDate() { return endDate;}
	public void setEndDate(String endDate) { this.endDate = endDate;}
	
	@OneToMany(fetch=FetchType.EAGER, cascade = {CascadeType.ALL})
	public Set<Race> getRaces() {
		return races;
	}
	
	public void setRaces(Set<Race> races) {
		this.races = races;
	}
	
}
```

### Race
```java
@Entity
@XmlRootElement(name="Race")
public class Race implements Serializable{
	private static final long serialVersionUID = 1L;
	int idRace;
	String name;
	String date;
	Set<Result> results = new HashSet<Result>();
	Set<Player> players = new HashSet<Player>();

	@Id
	@GeneratedValue
	@XmlAttribute
	public int getId() { return idRace;}
	public void setId(int id) { this.idRace = id;}
	
	public String getName() { return name;}
	public void setName(String name) { this.name = name;}
	public String getDate() { return date;}
	public void setDate(String date) { this.date = date;}
	
	@OneToMany(fetch=FetchType.EAGER,cascade = {CascadeType.ALL})
	public Set<Result> getResults() {
		return results;
	}
	
	public void setResults(Set<Result> results){
		this.results = results;
	}
	
    @ManyToMany(fetch=FetchType.EAGER,cascade = {CascadeType.ALL}) 
    @JoinTable(name="race_result", joinColumns=@JoinColumn(name="id_race"), inverseJoinColumns=@JoinColumn(name="id_result"))  
    public Set<Player> getPlayers()  {  
        return players;  
    }  
    
    public void setPlayers(Set<Player> players)  {  
        this.players = players;  
    }
}

```

### Player
```java
@Entity
@XmlRootElement
public class Player implements Serializable{
	private static final long serialVersionUID = 1L;
	int id;
	String firstname;
	String surname;
	int age;
	int sex;
	Set<Result> results = new HashSet<Result>();

	@Id
	@GeneratedValue
	@XmlAttribute
	public int getId() { return id;}
	public void setId(int id) { this.id = id;}
	public String getFirstname() { return firstname;}
	public void setFirstname(String firstname) { this.firstname = firstname;}
	public String getSurname() { return surname;}
	public void setSurname(String surname) { this.surname = surname;}
	public int getAge() { return age;}
	public void setAge(int age) { this.age = age;}
	
	@OneToMany
	public Set<Result> getResults() {
		return results;
	}
	
	public void setResults(Set<Result> results){
		this.results = results;
	}	
	
}

```

### Result
```java
@Entity
@XmlRootElement
public class Result implements Serializable{
	private static final long serialVersionUID = 1L;
	int idResult;
	double result;
	int position ;
	boolean accomplished;

	@Id
	@GeneratedValue
	@XmlAttribute
	public int getId() { return idResult;}
	public void setId(int id) { this.idResult = id;}
	public Double getResult() {	return result;}
	public void setResult(Double result) {	this.result = result;}
	public int getPosition() {return position;}
	public void setPosition(int position) {	this.position = position;}
	public boolean hasAccomplished() {	return accomplished;}
	public void setAccomplished(boolean accomplished) {	this.accomplished = accomplished;}

}

```

# Interfejs RESTowy

### Competition
#### Competition Interface
```java
@Local
public interface CompetitionInterface {

	public abstract String create(Competition competition);

	public abstract Competition find(int idc);

	public abstract Competitions get();

	public abstract String update(Competition competition);

	public abstract void delete(int idc);

}

```

#### CompetitionREST
```java
@Path("/competition")

@Consumes({ "application/xml" })
@Produces({ "application/xml" })

public class CompetitionREST implements CompetitionInterface {

	@EJB
	CompetitionEJB bean;

	@Override
	@POST
	@Path("/create")
	public String create(Competition competition) {
		bean.create(competition);
		return "competition created!";
	}

	@Override
	@GET
	@Path("/find/{idc}")
	public Competition find(@PathParam("idc") int idc) {
		Competition competition = bean.find(idc);
		return competition;
	}

	@Override
	@GET
	@Path("/get")
	public Competitions get() {
		List<Competition> lcompetitions = bean.get();
		Competitions competitions = new Competitions(lcompetitions);
		return competitions;
	}

	@Override
	@POST
	@Path("/update")
	public String update(Competition competition) {
		try {
			bean.update(competition);
			return "competition updated!";
		} catch (Exception e) {
			e.printStackTrace();
			return "competition not updated :(";
		}
	}


	@Override
	@GET
	@Path("/delete/{idc}")
	public void delete(@PathParam("idc") int idc) {
		bean.delete(idc);
	}

}

```

#### /create
* `POST`
```
<competition>
	<beginDate>string</beginDate>
	<endDate>string</endDate>
	<name>string</name>
</competition>
```

#### /update
* `POST`
```
<competition id="n">
	<beginDate>string</beginDate>
	<endDate>string</endDate>
	<name>string</name>
</competition>
```


#### /get
* `GET` 

##### Response
```
<competitions>
	<competition id="x">
		<beginDate>string</beginDate>
		<endDate>string</endDate>
		<name>string</name>
		<races>collectionOfRaces</races>
	</competition>
	<competition id="y">
		<beginDate>string</beginDate>
		<endDate>string</endDate>
		<name>string</name>
		<races>collectionOfRaces</races>
	</competition>
</competitions>
```

#### /find/{id}
* `GET`

##### Response
```
<competition id="id">
	<beginDate>string</beginDate>
	<endDate>string</endDate>
	<name>string</name>
	<races>collectionOfRaces</races>
</competition>
```

### Race
#### RaceInterface
```java
@Local
public interface RaceInterface {

	public abstract String create(Race race);

	public abstract Race find(int idc);

	public abstract Races get();

	public abstract String update(Race race);

	public abstract void delete(int idc);
}
```
#### RaceREST
```java
@Path("/race")
@Consumes({ "application/xml" })
@Produces({ "application/xml" })

public class RaceREST implements RaceInterface{
	@EJB
	RaceEJB bean;

	@Override
	@POST
	@Path("/create")
	public String create(Race race) {
		bean.create(race);
		return "Race created!";
	}

	@Override
	@GET
	@Path("/find/{idc}")
	public Race find(@PathParam("idc") int idc) {
		Race race = bean.find(idc);
		return race;
	}

	@Override
	@GET
	@Path("/get")
	public Races get() {
		List<Race> lraces = bean.get();
		Races races = new Races(lraces);
		return races;
	}

	@Override
	@POST
	@Path("/update")
	public String update(Race race) {
		try {
			bean.update(race);
			return "race updated!";
		} catch (Exception e) {
			e.printStackTrace();
			return "race not updated :(";
		}
	}


	@Override
	@GET
	@Path("/delete/{idc}")
	public void delete(@PathParam("idc") int idc) {
		bean.delete(idc);
	}
}
```
#### /create
* `POST`
```
<race>
	<name>string</name>
	<date>string</name>
	<results>list</results>
</race>
```

#### /update
* `POST`
```
<race id="n">
	<name>string</name>
	<date>string</name>
	<results>list</results>
</race>
```

#### /get
* `GET`

```
<races>
	<race id ="x">
		<name>string</name>
		<date>string</name>
		<results>collectionOfResults</results>
	</race>
	<race id ="x">
		<name>string</name>
		<date>string</name>
		<results>collectionOfResults</results>
	</race>
</races>
```

#### /find/{id}
* `GET`

```
<race id="id">
	<name>string</name>
	<date>string</name>
	<results>collectionOfResults<result>
	</results>
</race>
```

### Player
#### PlayerController
```java
@Local
public interface PlayerInterface {

	public abstract String create(Player obj);

	public abstract Player find(int idc);

	public abstract Players get();

	public abstract String update(Player obj);

	public abstract void delete(int idc);

}
```
#### PlayerREST
```java
@Path("/player")
@Consumes({ "application/xml" })
@Produces({ "application/xml" })

public class PlayerREST implements PlayerInterface {

	@EJB
	PlayerEJB bean;

	@Override
	@POST
	@Path("/create")
	public String create(Player obj) {
		bean.create(obj);
		return "competition created!";
	}

	@Override
	@GET
	@Path("/find/{idc}")
	public Player find(@PathParam("idc") int idc) {
		Player competition = bean.find(idc);
		return competition;
	}

	@Override
	@GET
	@Path("/get")
	public Players get() {
		List<Player> lcompetitions = bean.get();
		Players competitions = new Players(lcompetitions);
		return competitions;
	}

	@Override
	@POST
	@Path("/update")
	public String update(Player competition) {
		try {
			bean.update(competition);
			return "competition updated!";
		} catch (Exception e) {
			e.printStackTrace();
			return "competition not updated :(";
		}
	}


	@Override
	@GET
	@Path("/delete/{idc}")
	public void delete(@PathParam("idc") int idc) {
		bean.delete(idc);
	}

}
```

#### /create
* `POST`
```
<player>
	<firstname>string</firstname>
	<surname>string</surname>
	<age>int</age>
	<sex>int</sex>
	<results>collection</collection>
</player>
```

#### /update
* `POST`
```
<player id="n">
	<firstname>string</firstname>
	<surname>string</surname>
	<age>int</age>
	<sex>int</sex>
	<results>collectionOfResults</collection>
</player>
```

#### /get
* `GET`

```
<players>
	<player id="x">
		<firstname>string</firstname>
		<surname>string</surname>
		<age>int</age>
		<sex>int</sex>
		<results>collectionOfResults</collection>
	</player>
	<player id="y">
		<firstname>string</firstname>
		<surname>string</surname>
		<age>int</age>
		<sex>int</sex>
		<results>collectionOfResults</collection>
	</player>
	<player>...
</players>
```

#### /find/{id}
* `GET`

```
<player id="id">
	<firstname>string</firstname>
	<surname>string</surname>
	<age>int</age>
	<sex>int</sex>
	<results>collectionOfResults</collection>
</player>

```

### Result
#### ResultInterface
```java
@Local
public interface ResultInterface {

	public abstract String create(Result result);

	public abstract Result find(int idc);

	public abstract Results get();

	public abstract String update(Result result);

	public abstract void delete(int idc);
}
```
#### ResultRest
```java
@Path("/result")
@Consumes({ "application/xml" })
@Produces({ "application/xml" })

public class ResultREST implements ResultInterface{
	@EJB
	ResultEJB bean;

	@Override
	@POST
	@Path("/create")
	public String create(Result result) {
		bean.create(result);
		return "Race created!";
	}

	@Override
	@GET
	@Path("/find/{idc}")
	public Result find(@PathParam("idc") int idc) {
		Result result = bean.find(idc);
		return result;
	}

	@Override
	@GET
	@Path("/get")
	public Results get() {
		List<Result> lraces = bean.get();
		Results results = new Results(lraces);
		return results;
	}

	@Override
	@POST
	@Path("/update")
	public String update(Result result) {
		try {
			bean.update(result);
			return "race updated!";
		} catch (Exception e) {
			e.printStackTrace();
			return "race not updated :(";
		}
	}

	@Override
	@GET
	@Path("/delete/{idc}")
	public void delete(@PathParam("idc") int idc) {
		bean.delete(idc);
	}
}
```

#### /create
* `POST`
```
<result>
	<result>double</result>
	<position>int</position>
	<accomplished>boolean</accomplished>
<result>
```

#### /update
* `POST`
```
<result id="n">
	<result>double</result>
	<position>int</position>
	<accomplished>boolean</accomplished>
<result>
```

#### /get
* `GET`
```
<results>
	<result id="x">
		<result>double</result>
		<position>int</position>
		<accomplished>boolean</accomplished>
	</result>
	<result id="y">
		<result>double</result>
		<position>int</position>
		<accomplished>boolean</accomplished>
	</result>
	<result>...
</results>
```

#### /find/{id}
* `GET`

```
<result id="n">
	<result>double</result>
	<position>int</position>
	<accomplished>boolean</accomplished>
<result>
```