package pl.kurs.main;

import pl.kurs.interfaces.competition.CompetitionInterface;
import pl.kurs.interfaces.competition.CompetitionRemote;
import pl.kurs.views.competition.CompetitionTableModel;
import pl.kurs.views.competition.CompetitionWindow;

public class Start {
	public static void main(String[] args) {
		CompetitionTableModel model = new CompetitionTableModel();
		CompetitionInterface competitionInterface = new CompetitionRemote();
		
		model.setCompetitionDAO(competitionInterface);
		model.init();
		CompetitionWindow window = new CompetitionWindow(model);
		window.setVisible(true);
	}
}
