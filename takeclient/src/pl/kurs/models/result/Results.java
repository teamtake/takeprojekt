package pl.kurs.models.result;

import java.util.ArrayList;
import java.util.List;

public class Results {
	private List<Result> results = new ArrayList<Result>();

	public Results(List<Result> results) {
		super();
		this.results = results;
	}

	public Results() {	}
	
	public List<Result> getResults() {
		return results;
	}

	public void setResults(List<Result> results) {
		this.results = results;
	}
	
}