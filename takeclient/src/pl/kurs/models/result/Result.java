package pl.kurs.models.result;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

public class Result implements Serializable{
	private static final long serialVersionUID = 1L;
	int idResult;
	double result;
	int position ;
	boolean accomplished;

	@XmlAttribute
	public int getId() { return idResult;}
	public void setId(int id) { this.idResult = id;}
	public Double getResult() {	return result;}
	public void setResult(Double result) {	this.result = result;}
	public int getPosition() {return position;}
	public void setPosition(int position) {	this.position = position;}
	public boolean hasAccomplished() {	return accomplished;}
	public void setAccomplished(boolean accomplished) {	this.accomplished = accomplished;}

}
