package pl.kurs.models.race;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import pl.kurs.models.player.Player;
import pl.kurs.models.result.Result;

@XmlRootElement
public class Race implements Serializable{
	private static final long serialVersionUID = 1L;
	int idRace;
	String name;
	String date;
	Set<Result> results = new HashSet<Result>();
	Set<Player> players = new HashSet<Player>();

	@XmlAttribute
	public int getId() { return idRace;}
	public void setId(int id) { this.idRace = id;}
	
	public String getName() { return name;}
	public void setName(String name) { this.name = name;}
	public String getDate() { return date;}
	public void setDate(String date) { this.date = date;}
	
	public Set<Result> getResults() {
		return results;
	}
	 
    public Set<Player> getPlayers()  {  
        return players;  
    }  
    public void setPlayers(Set<Player> players)  {  
        this.players = players;  
    }
}
