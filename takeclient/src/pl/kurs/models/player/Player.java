package pl.kurs.models.player;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;

import pl.kurs.models.result.Result;

public class Player implements Serializable{
	private static final long serialVersionUID = 1L;
	int id;
	String firstname;
	String surname;
	int age;
	int sex;
	Set<Result> results = new HashSet<Result>();

	@XmlAttribute
	public int getId() { return id;}
	public void setId(int id) { this.id = id;}
	public String getFirstname() { return firstname;}
	public void setFirstname(String firstname) { this.firstname = firstname;}
	public String getSurname() { return surname;}
	public void setSurname(String surname) { this.surname = surname;}
	public int getAge() { return age;}
	public void setAge(int age) { this.age = age;}
	
	public Set<Result> getResults() {
		return results;
	}
	
}
