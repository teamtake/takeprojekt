package pl.kurs.models.competition;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import pl.kurs.models.race.Race;

@XmlRootElement(name="Competition")
public class Competition implements Serializable{
	private static final long serialVersionUID = 1L;
	int id;
	String name;
	String beginDate;
	String endDate;
	Set<Race> races = new HashSet<Race>();

	@XmlAttribute
	public int getId() { return id;}
	public void setId(int id) { this.id = id;}
	public String getName() { return name;}
	public void setName(String name) { this.name = name;}
	public String getBeginDate() { return beginDate;}
	public void setBeginDate(String beginDate) { this.beginDate = beginDate;}
	public String getEndDate() { return endDate;}
	public void setEndDate(String endDate) { this.endDate = endDate;}
	
	public Set<Race> getRaces() {
		return races;
	}
	
	public void setRaces(Set<Race> races) {
		this.races = races;
	}
	
}