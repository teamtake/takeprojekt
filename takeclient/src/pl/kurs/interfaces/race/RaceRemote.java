package pl.kurs.interfaces.race;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXB;

import pl.kurs.helpers.HttpHelper;
import pl.kurs.models.race.Race;
import pl.kurs.models.race.Races;

public class RaceRemote implements RaceInterface {

	String url = "http://localhost:8080/take/race";

	@Override
	public void create(Race race) {
		StringWriter sw = new StringWriter();
		JAXB.marshal(race, sw);
		HttpHelper.doPost(url+"/create",sw.toString(),"application/xml");
	}

	@Override
	public Race find(int idc) {
		String txt = HttpHelper.doGet(url+"/find/"+idc);
		Race r = JAXB.unmarshal(new StringReader(txt), Race.class);
		return r;
	}

	@Override
	public List<Race> get() {
		String txt = HttpHelper.doGet(url+"/get");
		Races races = JAXB.unmarshal(new StringReader(txt), Races.class);
		return races.getRaces();
	}

	@Override
	public void update(Race race) {
		StringWriter sw = new StringWriter();
		JAXB.marshal(race, sw);
		HttpHelper.doPost(url+"/update",sw.toString(),"application/xml");
	}

	@Override
	public void delete(int idc) {
		HttpHelper.doGet(url+"/delete/"+idc);
	}
}
