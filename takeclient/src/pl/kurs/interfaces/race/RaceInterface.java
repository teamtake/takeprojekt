package pl.kurs.interfaces.race;

import java.util.List;

import pl.kurs.models.race.Race;

public interface RaceInterface {
	public abstract void create(Race race);

	public abstract Race find(int idc);

	public abstract List<Race> get();

	public abstract void update(Race race);

	public abstract void delete(int idc);
}
