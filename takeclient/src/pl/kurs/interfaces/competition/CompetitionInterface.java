package pl.kurs.interfaces.competition;

import java.util.List;
import pl.kurs.models.competition.Competition;

public interface CompetitionInterface {
	public abstract void create(Competition competition);

	public abstract Competition find(int idc);

	public abstract List<Competition> get();

	public abstract void update(Competition competition);

	public abstract void delete(int idc);
}
