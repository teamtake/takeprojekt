package pl.kurs.interfaces.competition;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;
import javax.xml.bind.JAXB;

import pl.kurs.helpers.HttpHelper;
import pl.kurs.models.competition.Competition;
import pl.kurs.models.competition.Competitions;

public class CompetitionRemote implements CompetitionInterface{

	String url = "http://localhost:8080/take/competition";

	@Override
	public void create(Competition competition) {
		StringWriter sw = new StringWriter();
		JAXB.marshal(competition, sw);
		HttpHelper.doPost(url+"/create",sw.toString(),"application/xml");
	}

	@Override
	public Competition find(int idc) {
		String txt = HttpHelper.doGet(url+"/find/"+idc);
		Competition c = JAXB.unmarshal(new StringReader(txt), Competition.class);
		return c;
	}

	@Override
	public List<Competition> get() {
		String txt = HttpHelper.doGet(url+"/get");
		Competitions competitions = JAXB.unmarshal(new StringReader(txt), Competitions.class);
		return competitions.getCompetitions();
	}

	@Override
	public void update(Competition competition) {
		StringWriter sw = new StringWriter();
		JAXB.marshal(competition, sw);
		HttpHelper.doPost(url+"/update",sw.toString(),"application/xml");
	}

	@Override
	public void delete(int idc) {
		HttpHelper.doGet(url+"/delete/"+idc);
	}

}


