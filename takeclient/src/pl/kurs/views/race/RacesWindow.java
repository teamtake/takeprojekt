package pl.kurs.views.race;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import pl.kurs.interfaces.competition.CompetitionInterface;
import pl.kurs.interfaces.competition.CompetitionRemote;
import pl.kurs.interfaces.race.RaceInterface;
import pl.kurs.interfaces.race.RaceRemote;
import pl.kurs.models.competition.Competition;
import pl.kurs.views.competition.CompetitionTableModel;
import pl.kurs.views.result.ResultWindow;

public class RacesWindow extends JFrame{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JFrame previousWindow;
	private JButton backButton;
	private JButton btnAdd;
	private JButton btnDelete;
	private JButton btnDetails;
	private JLabel headerLabel;
	private JPanel headerPanel;
	private JPanel menuPanel;
	private JTable racesTable;
	private Competition competition;
	private RaceTableModel tableModel;
	private RaceInterface raceInterface;
	
	public RacesWindow(JFrame previousWindow, Competition competition){
		this.previousWindow = previousWindow;
		this.competition = competition;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
	
		getData();
		
		racesTable = new JTable(tableModel);
		createHeaderPanel();
		createMenuPanel();
		createMainPanel();
		
		this.pack();
		this.setLocationRelativeTo(previousWindow);
	}
	
	private void getData(){
		tableModel = new RaceTableModel();
		raceInterface = new RaceRemote();
		tableModel.setRaceDAO(raceInterface);
		tableModel.init(competition);
	}
	
	private void createHeaderPanel(){
		headerPanel = new JPanel();
		headerLabel = new JLabel(competition.getName() + ": Biegi");
		headerPanel.add(headerLabel);
	}
	
	private void createMenuPanel(){
		menuPanel = new JPanel();
		
		backButton = new JButton("Wr��");
		backButton.setPreferredSize(new Dimension(80,24));
		backButton.addActionListener(new ActionListener() {
			@Override	
			public void actionPerformed(ActionEvent e) {
				moveToPreviousWindow();
			}
		});
		
		menuPanel.add(backButton);
		btnAdd = new JButton("Dodaj");
		btnAdd.setPreferredSize(new Dimension(80,24));
		btnAdd.addActionListener(new ActionListener() {
			@Override	
			public void actionPerformed(ActionEvent e) {
				tableModel.addNew();
			}
		});
		menuPanel.add(btnAdd);
		
		btnDelete = new JButton("Usu�");
		btnDelete.setPreferredSize(new Dimension(80,24));
		btnDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int index = racesTable.getSelectedRow(); 
				if(index>=0) {
					tableModel.del(index);
				}
			}
		});
		menuPanel.add(btnDelete);
		
		btnDetails= new JButton("Wyniki");
		btnDetails.setPreferredSize(new Dimension(150,24));
		btnDetails.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				moveToNextWindow();	
			}
		});
		menuPanel.add(btnDetails);
	}
	
	private void createMainPanel(){
		contentPane = (JPanel) this.getContentPane();
		contentPane.add(new JScrollPane(racesTable),BorderLayout.CENTER);
		contentPane.add(menuPanel,BorderLayout.SOUTH);
		contentPane.add(headerPanel,BorderLayout.NORTH);
		setContentPane(contentPane);
	}
	
	private void moveToPreviousWindow(){
		previousWindow.setVisible(true);
		setVisible(false);
		dispose();
	}
	
	private void moveToNextWindow(){
		 int rowIndex = racesTable.getSelectedRow();
		 if(rowIndex >= 0){
			 new ResultWindow(this,tableModel.getRace(rowIndex)).setVisible(true);
			 setVisible(false);
			 dispose();
		 }
	}
}
