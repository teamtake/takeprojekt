package pl.kurs.views.race;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import pl.kurs.interfaces.competition.CompetitionInterface;
import pl.kurs.interfaces.competition.CompetitionRemote;
import pl.kurs.interfaces.race.RaceInterface;
import pl.kurs.interfaces.race.RaceRemote;
import pl.kurs.models.competition.Competition;
import pl.kurs.models.race.Race;

public class RaceTableModel extends AbstractTableModel{
	private static final long serialVersionUID = 1L;
	private RaceInterface raceDAO;
	private CompetitionInterface competitionDAO;
	private List<Race> races = new ArrayList<Race>();
	private Competition competition;
	
	public void setRaceDAO(RaceInterface raceDAO) {	
		this.raceDAO = raceDAO; 
		this.competitionDAO = new CompetitionRemote();
	}
	
	public void init(Competition competition) {
		this.competition = competition;
		races.addAll(competition.getRaces());
	}
	
	@Override
	public int getColumnCount() {
		return 2;
	}
	
	@Override
	public int getRowCount() {
		return races.size();
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		switch(columnIndex) {
			case 0: return "Nazwa";
			case 1: return "Data";
		}
		return "";
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0: return races.get(rowIndex).getName();
			case 1: return races.get(rowIndex).getDate();
		}
		return null;
	}
	
	@Override
	public void setValueAt(Object o,int rowIndex, int columnIndex) {
		try{
			Race race = races.get(rowIndex);
			String txt = (String)o;
			switch(columnIndex) {
				case 0: race.setName(txt);break;
				case 1: race.setDate(txt);break;
			}
			raceDAO.update(race);
			updateCompetition();
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, "B�edna warto�� wpisana do pola!","B��d",JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void add(Race race) {
		raceDAO.create(race);
		races.add(race);
		updateCompetition();
		fireTableDataChanged();
	}
	
	private void updateCompetition(){
		Set<Race> racesSet = new HashSet<Race>(races);
		competition.setRaces(racesSet);
		competitionDAO.update(competition);
	}
	
	public void addNew() {
		Race race = new Race();
		add(race);
	}
	
	public void del(int rowIndex) {
		Race race = races.get(rowIndex);
		races.remove(race);
		
		Set<Race> racesSet = new HashSet<Race>(races);
		competition.setRaces(racesSet);
		competitionDAO.update(competition);
		
		raceDAO.delete(race.getId());
	
		fireTableDataChanged();
	}
	
	public Race getRace(int index){
		return races.get(index);
	}
}
