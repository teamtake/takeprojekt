package pl.kurs.views.competition;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import pl.kurs.views.race.RacesWindow;

public class CompetitionWindow extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel menuPanel;
	private JButton btnAdd;
	private JButton btnDelete;
	private JButton btnDetails;
	private JLabel headerLabel;
	private JPanel headerPanel;
	private JTable competitionTable;
	private CompetitionTableModel tableModel;
	
	public CompetitionWindow(CompetitionTableModel tableModel) {
		this.tableModel = tableModel;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 200, 450, 300);

		createMenuPanel();
		competitionTable = new JTable(tableModel);
		createMainPanel();

		this.pack();
		this.setLocationRelativeTo(null);
	}
	
	private void createMenuPanel() {
		menuPanel = new JPanel();
		
		headerPanel = new JPanel();
		headerLabel = new JLabel("Zawody P�ywackie");
		headerPanel.add(headerLabel);

		btnAdd = new JButton("Dodaj");
		btnAdd.setPreferredSize(new Dimension(80,24));
		btnAdd.addActionListener(new ActionListener() {
			@Override	
			public void actionPerformed(ActionEvent e) {
				tableModel.addNew();
			}
		});
		menuPanel.add(btnAdd);
		
		btnDelete = new JButton("Usu�");
		btnDelete.setPreferredSize(new Dimension(80,24));
		btnDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int index = competitionTable.getSelectedRow(); 
				if(index>=0) {
					tableModel.del(index);
				}
			}
		});
		menuPanel.add(btnDelete);
		
		btnDetails= new JButton("Szczeg�y zawod�w");
		btnDetails.setPreferredSize(new Dimension(150,24));
		btnDetails.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				moveToNextWindow();	
			}
		});
		menuPanel.add(btnDetails);

	}
	
	private void createMainPanel(){
		contentPane = (JPanel) this.getContentPane();
		contentPane.add(new JScrollPane(competitionTable),BorderLayout.CENTER);
		contentPane.add(menuPanel,BorderLayout.SOUTH);
		contentPane.add(headerPanel,BorderLayout.NORTH);
		setContentPane(contentPane);
	}
	
	private void moveToNextWindow(){
		 int rowIndex = competitionTable.getSelectedRow();
		 if(rowIndex >= 0){
			 new RacesWindow(this,tableModel.getCompetition(rowIndex)).setVisible(true);
			 setVisible(false);
			 dispose();	 
		 }
	}

}
