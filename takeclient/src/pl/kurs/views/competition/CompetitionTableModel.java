package pl.kurs.views.competition;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import pl.kurs.interfaces.competition.CompetitionInterface;
import pl.kurs.models.competition.Competition;

public class CompetitionTableModel extends AbstractTableModel{
	private static final long serialVersionUID = 1L;
	private CompetitionInterface competitionDAO;
	private List<Competition> competitions = new ArrayList<Competition>();
	
	public void setCompetitionDAO(CompetitionInterface competitionDAO) {	
		this.competitionDAO = competitionDAO; 
	}
	
	public void init() {
		competitions = competitionDAO.get();
	}
	
	@Override
	public int getColumnCount() {
		return 3;
	}
	
	@Override
	public int getRowCount() {
		return competitions.size();
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		switch(columnIndex) {
			case 0: return "Nazwa";
			case 1: return "Data rozpocz�cia";
			case 2: return "Data zako�czenia";
		}
		return "";
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0: return competitions.get(rowIndex).getName();
			case 1: return competitions.get(rowIndex).getBeginDate();
			case 2: return competitions.get(rowIndex).getEndDate();
		}
		return null;
	}
	
	@Override
	public void setValueAt(Object o,int rowIndex, int columnIndex) {
		try{
			Competition competition = competitions.get(rowIndex);
			String txt = (String)o;
			switch(columnIndex) {
				case 0: competition.setName(txt);break;
				case 1: competition.setBeginDate(txt);break;
				case 2: competition.setEndDate(txt);break;
			}
			competitionDAO.update(competition);
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, "B��dna warto�� wpisana do pola!","B��d",JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void add(Competition competition) {
		competitionDAO.create(competition);
		competitions = competitionDAO.get();
		fireTableDataChanged();
	}
	
	public void addNew() {
		Competition competition = new Competition();
		add(competition);
	}

	public void del(int rowIndex) {
		Competition competition = competitions.get(rowIndex);
		competitionDAO.delete(competition.getId());
		competitions = competitionDAO.get();
		fireTableDataChanged();
	}
	
	public Competition getCompetition(int index){
		return competitions.get(index);
	}
}
